package ch3;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;


public class TestGridPane extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        GridPane root = new GridPane();
        root.setGridLinesVisible(true);
        root.setAlignment(Pos.CENTER);
        root.setMinSize(400,400);
        root.setPadding(new Insets(10,10,10,10));
        root.setVgap(10);
        root.setHgap(10);

        Label label0 = new Label("0");
        label0.setFont(Font.font(null, FontWeight.BOLD, 24));
        Label label1 = new Label("1");
        label1.setFont(Font.font(null, FontWeight.BOLD, 24));
        Label label2 = new Label("2");
        label2.setFont(Font.font(null, FontWeight.BOLD, 24));
        Label label3 = new Label("3");
        label3.setFont(Font.font(null, FontWeight.BOLD, 24));
        Label label = new Label("label_0_1");
        label.setFont(Font.font(null, FontWeight.BOLD, 24));

        GridPane.setColumnIndex(label0, 0);
        GridPane.setRowIndex(label0, 0);
        GridPane.setConstraints(label1, 1, 0);
        GridPane.setConstraints(label2, 2, 0);
        GridPane.setConstraints(label2, 3, 0);
        GridPane.setConstraints(label, 0, 1, 3, 1, HPos.CENTER, VPos.CENTER);

        root.getChildren().addAll(label, label0, label1, label2, label3);
        root.getColumnConstraints().forEach(columnConstraints -> columnConstraints.setPercentWidth(25));
        root.getRowConstraints().forEach(columnConstraints -> columnConstraints.setPercentHeight(25));

        Scene scene = new Scene(root, 400, 150);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
