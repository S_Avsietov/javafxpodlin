package ch3;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TestNodeOpacity extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox root = new VBox(30);
        root.setAlignment(Pos.CENTER);

        Label label = new Label("Измените значение с помощью ползунка");
        label.setStyle("-fx-font-size: 12pt;");

        Slider slider = new Slider(0, 1., .7);
        slider.setShowTickLabels(true);
        slider.setMajorTickUnit(.1);

        root.getChildren().addAll(label, slider);

        label.opacityProperty().bindBidirectional(slider.valueProperty());

        Scene scene = new Scene(root, 450, 150);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
