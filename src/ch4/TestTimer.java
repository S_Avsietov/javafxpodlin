package ch4;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

public class TestTimer extends Application {
    private Timer timer;

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);

        Label label = new Label();
        label.setFocusTraversable(true);
        label.setStyle("-fx-font-size: 40pt;");
        label.focusedProperty().addListener(
                (observable, oldValue, newValue) -> System.out.println(oldValue + " " + newValue));

        Button button = new Button("_Ok");
        TextField textField = new TextField();
        textField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER)
                button.requestFocus();
        });

        timer = new Timer(true);
        timer.schedule(new MyTimerTask(label), 0, 1000);

        root.getChildren().addAll(label, textField, button);

        Scene scene = new Scene(root, 400, 150);

        primaryStage.setTitle("Часы с точностью до секунды");
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(event -> {
            Alert dialog = new Alert(Alert.AlertType.CONFIRMATION, "Вы действтельно хотитет закрыть окно?");
            dialog.setTitle("Закрытие окна");
            dialog.setHeaderText(null);
            Optional<ButtonType> result = dialog.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK)
                System.out.println("Окно будет закрыто");
            else
                event.consume();
        });
        primaryStage.show();
    }

    private class MyTimerTask extends TimerTask {
        private Label label;

        public MyTimerTask(Label label) {
            this.label = label;
        }

        @Override
        public void run() {
            Platform.runLater(() ->
                    label.setText(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"))));
        }
    }
}
