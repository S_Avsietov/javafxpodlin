package ch12;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import java.util.function.UnaryOperator;


public class TestTextField extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        UnaryOperator<TextFormatter.Change> filter = change -> {
            String newState = change.getControlNewText();
            if (newState.matches("^\\d*$"))
               return change;
            else
                return null;
        };

        TextFormatter<String> textFormatter =
                new TextFormatter<>(TextFormatter.IDENTITY_STRING_CONVERTER, "0", filter);

        TextField onlyNumberTextField = new TextField();
        onlyNumberTextField.setPromptText("Введите цифры");
        onlyNumberTextField.setTextFormatter(textFormatter);

        Label label = new Label("_Только цифры: ");
        label.setLabelFor(onlyNumberTextField);
        label.setMnemonicParsing(true);

        Button button = new Button("Получить значение");
        button.setOnAction(event -> System.out.println(onlyNumberTextField.getText()));

        FlowPane root = new FlowPane(label, onlyNumberTextField, button);
        root.setAlignment(Pos.CENTER);
        root.setHgap(10);
        root.setVgap(10);

        Scene scene = new Scene(root, 450, 150);
        primaryStage.setTitle("Test TextField");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
