package ch12;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Spinner;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.StringConverter;

public class TestSpinner extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Spinner<Integer> spinner = new Spinner<>(0, 100, 5, 1);
        spinner.setEditable(true);
        spinner.getValueFactory().setWrapAround(true);
        spinner.getValueFactory().setConverter(
                new StringConverter<Integer>() {
                    @Override
                    public String toString(Integer object) {
                        return (object == null ? "0" : object.toString());
                    }

                    @Override
                    public Integer fromString(String string) {
                        if (string.matches("^[0-9]+$"))
                            try {
                                return Integer.valueOf(string);
                            } catch (NumberFormatException e) {
                                return 0;
                            }
                        return 0;
                    }
                }
        );

        StackPane root = new StackPane(spinner);
        Scene scene = new Scene(root, 450, 150);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Test Spinner");
        primaryStage.show();
    }
}
