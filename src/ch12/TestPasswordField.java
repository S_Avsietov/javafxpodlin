package ch12;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class TestPasswordField extends Application {
    public static void main(String[] args){
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        PasswordField passwordField=new PasswordField();
        FlowPane root=new FlowPane(passwordField);
        root.setAlignment(Pos.CENTER);

        Scene scene=new Scene(root,450,150);
        primaryStage.setTitle("Test PasswordField");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
