package ch10;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.DrawMode;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;
import javafx.scene.shape.VertexFormat;
import javafx.stage.Stage;

public class TestMesh extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        TriangleMesh pyramidMesh = new TriangleMesh(VertexFormat.POINT_TEXCOORD);
        float h = 150;                    // Height
        float s = 300;                    // Side
        pyramidMesh.getTexCoords().addAll(0, 0
        );
        pyramidMesh.getPoints().addAll(
                0, 0, 0,            // Point 0 - Top
                0, h, -s / 2,         // Point 1 - Front
                -s / 2, h, 0,            // Point 2 - Left
                s / 2, h, 0,            // Point 3 - Back
                0, h, s / 2           // Point 4 - Right
        );
        pyramidMesh.getFaces().addAll(
                0, 0, 2, 0, 1, 0,          // Front left face
                0, 0, 1, 0, 3, 0,          // Front right face
                0, 0, 3, 0, 4, 0,          // Back right face
                0, 0, 4, 0, 2, 0,          // Back left face
                4, 0, 1, 0, 2, 0,          // Bottom rear face
                4, 0, 3, 0, 1, 0           // Bottom front face
        );

        PhongMaterial material = new PhongMaterial(Color.BLUEVIOLET);
        material.setDiffuseMap(new Image(TestMesh.class.getResourceAsStream("photo.png")));

        MeshView pyramid = new MeshView(pyramidMesh);
        pyramid.setDrawMode(DrawMode.FILL);
        pyramid.setMaterial(material);
        pyramid.setTranslateX(200);
        pyramid.setTranslateY(100);
        pyramid.setTranslateZ(200);

        //Creating a Group object
        Group root = new Group(pyramid);

        //Creating a scene object
        Scene scene = new Scene(root, 400, 400, true, SceneAntialiasing.BALANCED);

        //Setting title to the Stage
        primaryStage.setTitle("Drawing a Box");

        //Adding scene to the stage
        primaryStage.setScene(scene);

        //Displaying the contents of the stage
        primaryStage.show();
    }
}
