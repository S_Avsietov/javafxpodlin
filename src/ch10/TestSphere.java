package ch10;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.DrawMode;
import javafx.scene.shape.Sphere;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;

public class TestSphere extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //Drawing a Sphere
        Sphere sphere = new Sphere(100);

        //Setting the properties of the Cylinder
        //cylinder.setRadius(100);
        //cylinder.setHeight(200);
        sphere.setDrawMode(DrawMode.FILL);
        sphere.setTranslateX(200);
        sphere.setTranslateY(200);
        sphere.setMaterial(new PhongMaterial(Color.PALEGREEN));

        //Creating a Group object
        Group root = new Group(sphere);

        //Creating a scene object
        Scene scene = new Scene(root, 400, 400);

        //Setting title to the Stage
        primaryStage.setTitle("Drawing a Box");

        //Adding scene to the stage
        primaryStage.setScene(scene);

        //Displaying the contents of the stage
        primaryStage.show();
    }
}
