package ch10;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.DrawMode;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;

public class TestCylinder extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //Drawing a TestCylinder
        Cylinder cylinder = new Cylinder(100,200);

        //Setting the properties of the Cylinder
        //cylinder.setRadius(100);
        //cylinder.setHeight(200);
        cylinder.setDrawMode(DrawMode.FILL);
        cylinder.setTranslateX(200);
        cylinder.setTranslateY(200);
        cylinder.setMaterial(new PhongMaterial(Color.PALEGREEN));

        Rotate rxBox = new Rotate(0, 0, 0, 0, Rotate.X_AXIS);
        Rotate ryBox = new Rotate(0, 0, 0, 0, Rotate.Y_AXIS);
        Rotate rzBox = new Rotate(0, 0, 0, 0, Rotate.Z_AXIS);
        rxBox.setAngle(15);
        ryBox.setAngle(15);
        rzBox.setAngle(15);
        cylinder.getTransforms().addAll(rxBox, ryBox, rzBox);

        //Creating a Group object
        Group root = new Group(cylinder);

        //Creating a scene object
        Scene scene = new Scene(root, 400, 400);

        //Setting title to the Stage
        primaryStage.setTitle("Drawing a Box");

        //Adding scene to the stage
        primaryStage.setScene(scene);

        //Displaying the contents of the stage
        primaryStage.show();
    }
}
