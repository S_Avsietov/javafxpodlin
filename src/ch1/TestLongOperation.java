package ch1;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Замечание
 * Изменять свойства компонент можно только из основого потока JavaFX Application Thread
 * Для изменения свойств из друго потока нужно сформировать задачу Runnable
 * и передать ее статическому методу runLater класса Platform
 */

public class TestLongOperation extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox root = new VBox();
        Button wrongRunProcessButton = new Button("Запустить процесс неправильно в основном потоке");
        wrongRunProcessButton.setOnAction(event -> {
            try {
                Thread.sleep(10000);
                System.out.println("End long operation");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Button rightRunProcessButton = new Button("Запустить процесс правильно в отдельном потоке");
        rightRunProcessButton.setOnAction(event -> {
            (new Thread(() -> {
                try {
                    Thread.sleep(10000);
                    System.out.println("End long operation");
                } catch (InterruptedException e) {
                }
            })).start();
        });
        root.getChildren().addAll(wrongRunProcessButton,rightRunProcessButton);

        Scene scene = new Scene(root, 500, 150);

        primaryStage.setTitle("Заголовок окна");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
