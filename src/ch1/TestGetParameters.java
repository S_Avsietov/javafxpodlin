package ch1;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class TestGetParameters extends Application {
    public static void main(String[] args) {
        args = new String[]{"value1", "--key2 = value2"};
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane root = new BorderPane();
        Label text = new Label("Привет мир!");
        text.setFont(new Font(32));
        root.setCenter(text);

        Scene scene = new Scene(root, 400, 150);

        primaryStage.setTitle("Заголовок окна");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void init() throws Exception {
        //Можно вызывать начиная с метода init(), не раньше
        System.out.println("getRaw(): " + getParameters().getRaw());
        System.out.println("getNamed(): " + getParameters().getNamed());
        System.out.println("getUnnamed(): " + getParameters().getUnnamed());
    }
}
