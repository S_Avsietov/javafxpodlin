package ch1;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class TestExitApp extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane root = new BorderPane();
        Button stopButton = new Button("Закрыть окно");
        stopButton.setOnAction(event -> Platform.exit());
        //stopButton.setOnAction(event -> System.exit(0)); //Не вызовится методо stop()
        root.setCenter(stopButton);

        Scene scene = new Scene(root, 400, 150);

        primaryStage.setTitle("Заголовок окна");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        System.out.println("Метод stop()");
    }
}
