package ch1;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class TestInitAndStop extends Application {
    public static void main(String[] args) {
        System.out.println("Метод main " + Thread.currentThread().getName());
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println("Метод start " + Thread.currentThread().getName());
        BorderPane root = new BorderPane();
        Label text = new Label("Привет мир!");
        text.setFont(new Font(32));
        root.setCenter(text);

        Scene scene = new Scene(root, 400, 150);

        primaryStage.setTitle("Заголовок окна");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void init() throws Exception {
        System.out.println("Метод init " + Thread.currentThread().getName());
    }

    @Override
    public void stop() throws Exception {
        System.out.println("Метод stop " + Thread.currentThread().getName());
    }
}
