package ch2;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class TestModality extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane root = new BorderPane();
        Button button = new Button("Открыть окно");
        button.setOnAction(event -> {
            //newWindow(primaryStage, Modality.NONE);
            //newWindow(primaryStage, Modality.WINDOW_MODAL);
            newWindow(primaryStage, Modality.APPLICATION_MODAL);
            System.out.println("Окно закрыто");
        });
        root.setCenter(button);

        Scene scene = new Scene(root, 400, 150);

        primaryStage.setTitle("Создание нового окна");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void newWindow(Stage parent, Modality modality) {
        Stage window = new Stage();
        window.setTitle("Новое окно");
        window.initModality(modality);
        window.initOwner(parent);

        Button button = new Button("Закррыть окно");
        button.setOnAction(event -> {
            window.close();
        });

        BorderPane pane = new BorderPane();
        pane.setCenter(button);

        Scene scene = new Scene(pane, 400, 150);

        window.setScene(scene);
        //window.show();
        window.showAndWait();
    }
}
