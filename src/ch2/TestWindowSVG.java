package ch2;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class TestWindowSVG extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane root = new BorderPane();
        Button button = new Button("Открыть окно");
        button.setOnAction(event -> {
            newWindow(primaryStage);
            System.out.println("Окно закрыто");
        });
        root.setCenter(button);

        Scene scene = new Scene(root, 400, 150);

        primaryStage.setTitle("Создание нового окна");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void newWindow(Stage parent) {
        Stage window = new Stage(StageStyle.TRANSPARENT);
        window.setTitle("Окно произвольной формы");

        SVGPath svgPath = new SVGPath();
        svgPath.setContent("M 50 50 L 350 50 300 250 L 200 200 L 100 250 z");
        svgPath.setFill(new Color(0.0, 1.0, 0.0, 1.0));
        svgPath.setEffect(new DropShadow());

        Button button = new Button("Закрыть окно");
        button.setOnAction(event -> {
            window.close();
        });

        StackPane pane = new StackPane();
        pane.getChildren().addAll(svgPath, button);
        pane.setBackground(Background.EMPTY);

        Scene scene = new Scene(pane, 400, 150, Color.TRANSPARENT);

        window.setScene(scene);
        //window.show();
        window.showAndWait();
    }
}
