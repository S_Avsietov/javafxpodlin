package ch2;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.Optional;

public class TestCloseWIndow extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox root = new VBox(15);
        root.setAlignment(Pos.CENTER);

        Button button = new Button("Закрыть окно");
        button.setOnAction(event -> primaryStage.close());

        Label label = new Label("Нажмите кнопку Закрыть в заголовке окна");

        root.getChildren().addAll(label, button);

        primaryStage.setOnCloseRequest(event -> {
            Alert dialog = new Alert(AlertType.CONFIRMATION, "ВЫ действительно хотите закрыть окно?");
            dialog.setTitle("Закрытие окна");
            dialog.setHeaderText(null);
            Optional<ButtonType> result = dialog.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK)
                System.out.println("Окно будет закрыто");
            else {
                System.out.println("Окно не будет закрыто");
                event.consume();
            }
        });
        primaryStage.setTitle("Заурытие окна");
        primaryStage.setScene(new Scene(root, 450, 150));
        primaryStage.show();
    }
}
