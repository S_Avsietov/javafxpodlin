package ch2;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.nio.file.StandardCopyOption;


public class TestWindow extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane root = new BorderPane();
        Button button = new Button("Открыть окно");
        button.setOnAction(event -> {
            newWindow(primaryStage);
            System.out.println("Окно закрыто");
        });
        root.setCenter(button);

        Scene scene = new Scene(root, 400, 150);

        primaryStage.setTitle("Создание нового окна");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void newWindow(Stage parent) {
        Stage window = new Stage();
        window.setTitle("Новое окно");

        Button button = new Button("Скрыть или отобразить главное окно");
        button.setOnAction(event -> {
            if (parent.isShowing()) parent.hide();
            else parent.show();
        });

        BorderPane pane = new BorderPane();
        pane.setCenter(button);

        Scene scene = new Scene(pane, 400, 150);

        window.setScene(scene);
        //window.show();
        window.showAndWait();
    }
}
