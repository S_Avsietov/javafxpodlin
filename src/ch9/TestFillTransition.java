package ch9;

import javafx.animation.Animation;
import javafx.animation.FillTransition;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

public class TestFillTransition extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        javafx.scene.shape.Rectangle rect = new Rectangle(200, 100, 200, 200);
        rect.setArcWidth(50);
        rect.setArcHeight(50);
        rect.setFill(Color.TEAL);

        FillTransition fillTransition = new FillTransition(Duration.seconds(5), rect, Color.RED, Color.BLUE);
        fillTransition.setAutoReverse(true);
        fillTransition.setCycleCount(5);

        Group group = new Group();
        group.getChildren().addAll(rect);
        group.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (fillTransition.getStatus() != Animation.Status.RUNNING) fillTransition.play();
        });

        Scene scene = new Scene(group, 600, 400);

        primaryStage.setTitle("Test RotateTransition");
        primaryStage.setScene(scene);
        primaryStage.show();
        fillTransition.play();
    }
}
