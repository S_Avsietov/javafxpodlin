package ch9;

import javafx.animation.ScaleTransition;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;


public class TestScaleTransition extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Rectangle rect = new Rectangle(250, 150, 100, 100);
        rect.setArcHeight(30);
        rect.setArcWidth(30);
        rect.setFill(Color.CORNFLOWERBLUE);

        ScaleTransition scaleTransition = new ScaleTransition(Duration.seconds(5), rect);
        scaleTransition.setAutoReverse(true);
        scaleTransition.setByX(2);
        scaleTransition.setByY(2);
        //scaleTransition.setToX(4);
        //scaleTransition.setToY(4);
        scaleTransition.setCycleCount(4);

        Group group = new Group();
        group.getChildren().addAll(rect);

        Scene scene = new Scene(group, 600, 400);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Test ScaleTransformation");
        primaryStage.show();

        scaleTransition.play();
    }
}
