package ch9;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class TestAnimationTimer extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Rectangle rect = new Rectangle(100, 40, 100, 100);
        rect.setArcHeight(50);
        rect.setArcWidth(50);
        rect.setFill(Color.VIOLET);

        AnimationTimer animationTimer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                if (rect.getTranslateX() > 300)
                    rect.setTranslateX(0);
                else
                    rect.setTranslateX(rect.getTranslateX() + 1.);
            }
        };

        Group root = new Group();
        root.getChildren().addAll(rect);
        Scene scene = new Scene(root, 600, 400);
        primaryStage.setTitle("Test TranslateTransition");
        primaryStage.setScene(scene);
        primaryStage.show();
        animationTimer.start();
    }
}
