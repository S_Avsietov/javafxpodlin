package ch9;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class TestTransition extends Application {
    private static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Text text = new Text();
        text.setFont(Font.font(null, FontWeight.BOLD, 50));
        String content = "Lorem Ipsum";

        Animation animation = new Transition() {
            {
                setCycleDuration(Duration.seconds(2));
                setInterpolator(Interpolator.LINEAR);
            }

            @Override
            protected void interpolate(double frac) {
                int length = content.length();
                int endPos = (int) (length * frac);
                text.setText(content.substring(0, endPos));
            }
        };

        FlowPane root = new FlowPane();
        root.setAlignment(Pos.CENTER);

        root.getChildren().addAll(text);
        Scene scene = new Scene(root, 450, 150);
        scene.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (animation.getStatus() != Animation.Status.RUNNING)
                animation.play();
        });

        primaryStage.setTitle("Test Transition");
        primaryStage.setScene(scene);
        primaryStage.show();
        animation.play();

    }
}
