package ch9;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

public class TestTimeline extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Rectangle rect = new Rectangle(100, 40, 100, 100);
        rect.setArcHeight(50);
        rect.setArcWidth(50);
        rect.setFill(Color.RED);

        KeyFrame keyFrame0 = new KeyFrame(Duration.ZERO
                , "First Frame"
                , new KeyValue(rect.opacityProperty(), 1)
                , new KeyValue(rect.translateXProperty(), 0));
        KeyFrame keyFrame1 = new KeyFrame(Duration.seconds(5)
                , event -> System.out.println("keyFrame2 finished")
                , new KeyValue(rect.opacityProperty(), .3)
                , new KeyValue(rect.translateXProperty(), 300));

        Timeline timeline = new Timeline(keyFrame0, keyFrame1);
        timeline.setAutoReverse(true);
        timeline.setCycleCount(10);

        Group group = new Group();
        group.getChildren().addAll(rect);
        group.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (timeline.getStatus() != Animation.Status.RUNNING) timeline.play();
        });

        Scene scene = new Scene(group, 600, 400);

        primaryStage.setTitle("Test RotateTransition");
        primaryStage.setScene(scene);
        primaryStage.show();
        timeline.play();
    }
}
