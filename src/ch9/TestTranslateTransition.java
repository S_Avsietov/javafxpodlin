package ch9;

import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

public class TestTranslateTransition extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Rectangle rect = new Rectangle(100, 40, 100, 100);
        rect.setArcHeight(50);
        rect.setArcWidth(50);
        rect.setFill(Color.VIOLET);

        TranslateTransition tt = new TranslateTransition(Duration.millis(2000), rect);
        //tt.setByX(200f);
        tt.setFromX(0);
        tt.setFromY(0);
        tt.setToX(400);
        tt.setToY(260);
        tt.setCycleCount(4);
        tt.setAutoReverse(true);

        Group root = new Group();
        root.getChildren().addAll(rect);
        Scene scene = new Scene(root, 600, 400);
        primaryStage.setTitle("Test TranslateTransition");
        primaryStage.setScene(scene);
        primaryStage.show();
        tt.play();
    }
}
