package ch9;

import javafx.animation.Animation;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;


public class TestFadeTransition extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Rectangle rect = new Rectangle(100, 40, 100, 100);
        rect.setArcHeight(50);
        rect.setArcWidth(50);
        rect.setFill(Color.CORNFLOWERBLUE);

        FadeTransition ft = new FadeTransition(Duration.millis(3000), rect);
        ft.setFromValue(1.0);
        ft.setToValue(0.3);
        ft.setCycleCount(4);
        ft.setAutoReverse(true);

        Button button = new Button("run/pause");
        button.setOnAction(event -> {
            if (ft.getStatus() == Animation.Status.RUNNING) ft.pause();
            else ft.play();
        });

        rect.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (ft.getStatus() == Animation.Status.RUNNING) ft.pause();
            else ft.play();
        });

        VBox root = new VBox(30);
        root.setAlignment(Pos.CENTER);
        root.getChildren().addAll(rect, button);

        Scene scene = new Scene(root, 200, 200);

        primaryStage.setTitle("Test FadeTransition");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
