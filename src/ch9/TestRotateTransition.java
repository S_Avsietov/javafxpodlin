package ch9;

import javafx.animation.Animation;
import javafx.animation.RotateTransition;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;


public class TestRotateTransition extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Rectangle rect = new Rectangle(250, 150, 100, 100);
        rect.setArcWidth(50);
        rect.setArcHeight(50);
        rect.setFill(Color.TEAL);

        RotateTransition rotateTransition = new RotateTransition(Duration.seconds(1), rect);
        rotateTransition.setAutoReverse(true);
        rotateTransition.setByAngle(360);
        rotateTransition.setAxis(Rotate.X_AXIS);

        Group group = new Group();
        group.getChildren().addAll(rect);
        group.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (rotateTransition.getStatus() != Animation.Status.RUNNING) rotateTransition.play();
        });

        Scene scene = new Scene(group, 600, 400);

        primaryStage.setTitle("Test RotateTransition");
        primaryStage.setScene(scene);
        primaryStage.show();
        rotateTransition.play();
    }
}
