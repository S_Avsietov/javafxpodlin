package ch8;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.Shadow;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class TestShadow extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        HBox root = new HBox(30);
        root.setAlignment(Pos.CENTER);

        Shadow shadow = new Shadow(15, Color.BLUE);

        Label label = new Label("Shadow");
        label.setFont(Font.font(null, FontWeight.BOLD, 50));
        label.setEffect(shadow);

        root.getChildren().addAll(label);

        Scene scene = new Scene(root, 450, 150);

        primaryStage.setTitle("Test Shadow");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
