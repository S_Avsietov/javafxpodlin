package ch8;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.BoxBlur;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class TestBoxBlur extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane root = new BorderPane();

        BoxBlur boxBlur = new BoxBlur(5, 5, 3);

        Label label = new Label("Box Blur");
        label.setFont(Font.font(null, FontWeight.BOLD, 50));
        label.setEffect(boxBlur);

        root.setCenter(label);

        Scene scene=new Scene(root,450,150);

        primaryStage.setTitle("Test Box Blur");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
