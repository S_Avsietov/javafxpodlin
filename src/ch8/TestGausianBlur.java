package ch8;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;


public class TestGausianBlur extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        HBox root = new HBox(30);
        root.setAlignment(Pos.CENTER);

        GaussianBlur gaussianBlur = new GaussianBlur();

        Label label = new Label("Gausian Blur");
        label.setFont(Font.font(null, FontWeight.BOLD, 50));
        label.setEffect(gaussianBlur);

        root.getChildren().addAll(label);

        Scene scene = new Scene(root, 450, 150);

        primaryStage.setTitle("Test Gausian Blur");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
