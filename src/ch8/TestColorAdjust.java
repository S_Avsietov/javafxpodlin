package ch8;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

public class TestColorAdjust extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FlowPane root = new FlowPane();
        root.setAlignment(Pos.CENTER);

        ColorAdjust colorAdjust = new ColorAdjust();
        colorAdjust.setContrast(0.1);
        colorAdjust.setHue(-.05);
        colorAdjust.setBrightness(0.3);
        colorAdjust.setSaturation(0.2);

        Image image = new Image(TestColorAdjust.class.getResourceAsStream("ch10/photo.png"));

        ImageView imageViewBefore = new ImageView(image);
        imageViewBefore.setFitWidth(200);
        imageViewBefore.setPreserveRatio(true);

        ImageView imageView = new ImageView(image);
        imageView.setFitWidth(200);
        imageView.setPreserveRatio(true);
        imageView.setEffect(colorAdjust);

        root.getChildren().addAll(imageViewBefore, imageView);

        Scene scene = new Scene(root, 450, 150);
        primaryStage.setTitle("Test Color Adjust");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
