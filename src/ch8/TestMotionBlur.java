package ch8;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.MotionBlur;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class TestMotionBlur extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        HBox root = new HBox(30);
        root.setAlignment(Pos.CENTER);

        MotionBlur motionBlur = new MotionBlur(-15, 30);

        Label label = new Label("Motion Blur");
        label.setFont(Font.font(null, FontWeight.BOLD, 50));
        label.setEffect(motionBlur);
        label.setTextFill(Color.BLUEVIOLET);

        root.getChildren().addAll(label);

        Scene scene = new Scene(root, 450, 150);

        primaryStage.setTitle("Test Motion Blur");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
