package ch8;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class TestInnerShadow extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox root = new VBox(30);
        root.setAlignment(Pos.CENTER);

        InnerShadow innerShadow = new InnerShadow();
        innerShadow.setOffsetX(4);
        innerShadow.setOffsetY(4);
        innerShadow.setColor(Color.web("0x3b596d"));

        Label label = new Label("Inner Shadow");
        label.setFont(Font.font(null, FontWeight.BOLD, 50));
        label.setTextFill(Color.ALICEBLUE);
        label.setEffect(innerShadow);

        root.getChildren().addAll(label);

        Scene scene = new Scene(root, 400, 150);

        primaryStage.setTitle("Test Inner Shadow");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
