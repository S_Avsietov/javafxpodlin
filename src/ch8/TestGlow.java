package ch8;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.effect.Glow;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class TestGlow extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Pane root = new Pane();

        Glow glow = new Glow(0.9);


        Rectangle rect = new Rectangle();
        rect.setX(10);
        rect.setY(10);
        rect.setWidth(160);
        rect.setHeight(80);
        rect.setFill(Color.DARKSLATEBLUE);

        Text text = new Text();
        text.setText("Glow!");
        text.setFill(Color.ALICEBLUE);
        text.setFont(Font.font(null, FontWeight.BOLD, 40));
        text.setX(25);
        text.setY(65);
        text.setEffect(glow);

        root.getChildren().addAll(rect, text);

        Scene scene = new Scene(root, 450, 150);

        primaryStage.setTitle("Test Glow");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
