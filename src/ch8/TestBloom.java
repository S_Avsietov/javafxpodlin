package ch8;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.effect.Bloom;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class TestBloom extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Pane root = new Pane();

        Bloom bloom = new Bloom();
        bloom.setThreshold(.5);

        Rectangle rect = new Rectangle();
        rect.setX(10);
        rect.setY(10);
        rect.setWidth(160);
        rect.setHeight(80);
        rect.setFill(Color.DARKSLATEBLUE);

        Text text = new Text();
        text.setText("Bloom!");
        text.setFill(Color.ALICEBLUE);
        text.setFont(Font.font(null, FontWeight.BOLD, 40));
        text.setX(25);
        text.setY(65);
        text.setEffect(bloom);

        root.getChildren().addAll(rect, text);

        Scene scene = new Scene(root, 450, 150);

        primaryStage.setTitle("Test Box Blur");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
