package ch8;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.effect.DisplacementMap;
import javafx.scene.effect.FloatMap;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class TestDisplacementMap extends Application {
    public static void main(String[] args){
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        int width = 220;
        int height = 100;

        FloatMap floatMap = new FloatMap();
        floatMap.setWidth(width);
        floatMap.setHeight(height);

        for (int i = 0; i < width; i++) {
            double v = (Math.sin(i / 20.0 * Math.PI) - 0.5) / 40.0;
            for (int j = 0; j < height; j++) {
                floatMap.setSamples(i, j, 0.0f, (float) v);
            }
        }

        DisplacementMap displacementMap = new DisplacementMap();
        displacementMap.setMapData(floatMap);

        Text text = new Text();
        text.setX(40.0);
        text.setY(80.0);
        text.setText("Wavy Text");
        text.setFill(Color.web("0x3b596d"));
        text.setFont(Font.font(null, FontWeight.BOLD, 50));
        text.setEffect(displacementMap);

        FlowPane root=new FlowPane();
        root.setAlignment(Pos.CENTER);
        root.getChildren().addAll(text);
        Scene scene=new Scene(root,450,150);
        primaryStage.setTitle("Test DisplacementMap");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
