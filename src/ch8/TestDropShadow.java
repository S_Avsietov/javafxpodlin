package ch8;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class TestDropShadow extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        HBox root = new HBox(30);
        root.setAlignment(Pos.CENTER);

        DropShadow dropShadow = new DropShadow();
        dropShadow.setRadius(10.0);
        dropShadow.setOffsetX(10.0);
        dropShadow.setOffsetY(10.0);
        dropShadow.setColor(Color.color(0.4, 0.5, 0.5));

        Label label = new Label("Drop Shadow");
        label.setFont(Font.font(null, FontWeight.BOLD, 50));
        label.setEffect(dropShadow);

        root.getChildren().addAll(label);

        Scene scene = new Scene(root, 450, 150);

        primaryStage.setTitle("Test Drop Shadow");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
