package ch8;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class TestReflection extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        HBox root = new HBox(30);
        root.setAlignment(Pos.CENTER);

        Reflection reflection = new Reflection(-20, 1, .5, 0);
        //reflection.setFraction(.7);

        Label label = new Label("Reflection");
        label.setFont(Font.font(null, FontWeight.BOLD, 50));
        label.setEffect(reflection);

        root.getChildren().addAll(label);

        Scene scene = new Scene(root, 450, 300);

        primaryStage.setTitle("Test Reflection");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
