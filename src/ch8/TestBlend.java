package ch8;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.effect.Blend;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.ColorInput;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class TestBlend extends Application {
    public static void main(String[] args){
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Blend blend = new Blend();
        blend.setMode(BlendMode.COLOR_BURN);

        ColorInput colorInput = new ColorInput();
        colorInput.setPaint(Color.STEELBLUE);
        colorInput.setX(10);
        colorInput.setY(10);
        colorInput.setWidth(100);
        colorInput.setHeight(180);

        blend.setTopInput(colorInput);

        Rectangle rect = new Rectangle();
        rect.setWidth(220);
        rect.setHeight(100);
        Stop[] stops = new Stop[]{new Stop(0, Color.LIGHTSTEELBLUE), new Stop(1, Color.PALEGREEN)};
        LinearGradient lg = new LinearGradient(0, 0, 0.25, 0.25, true, CycleMethod.REFLECT, stops);
        rect.setFill(lg);

        Text text = new Text();
        text.setX(15);
        text.setY(65);
        text.setFill(Color.PALEVIOLETRED);
        text.setText("COLOR_BURN");
        text.setFont(Font.font(null, FontWeight.BOLD, 30));

        Group g = new Group();
        g.setEffect(blend);
        g.getChildren().addAll(rect, text);

        Scene scene=new Scene(g,450,250);

        primaryStage.setTitle("Test Blend");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
