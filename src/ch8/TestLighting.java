package ch8;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class TestLighting extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FlowPane root = new FlowPane();
        root.setAlignment(Pos.CENTER);

        Light.Distant light = new Light.Distant();
        light.setAzimuth(-135.0);

        Lighting lighting = new Lighting();
        lighting.setLight(light);
        lighting.setSurfaceScale(5.0);
        lighting.setDiffuseConstant(2.);
        lighting.setSpecularConstant(2.);

        Text text = new Text();
        text.setText("JavaFX!");
        text.setFill(Color.STEELBLUE);
        text.setFont(Font.font(null, FontWeight.BOLD, 60));
        text.setX(10.0);
        text.setY(10.0);
        text.setTextOrigin(VPos.TOP);
        text.setEffect(lighting);

        root.getChildren().addAll(text);

        Scene scene = new Scene(root, 450, 150);

        primaryStage.setTitle("Test Lighting");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
