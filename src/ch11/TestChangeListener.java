package ch11;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class TestChangeListener extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Rectangle rect = new Rectangle(0, 0, 100, 100);
        rect.setFill(Color.RED);
        rect.setArcHeight(10);
        rect.setArcWidth(10);

        Pane pane = new Pane();
        pane.getChildren().addAll(rect);

        Slider slider = new Slider(0, 350, 1);
        slider.setShowTickMarks(true);
        slider.setMajorTickUnit(1);
        slider.valueProperty().addListener((observable, oldValue, newValue) ->
                rect.setTranslateX(newValue.doubleValue()));

        VBox root = new VBox(30);
        root.setFillWidth(true);
        root.getChildren().addAll(pane, slider);

        Scene scene = new Scene(root, 450, 150);

        primaryStage.setTitle("Test Change Listener");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
