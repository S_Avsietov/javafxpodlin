package ch11;

import javafx.animation.PauseTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Duration;

public class TestTask extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox root = new VBox(30);
        root.setAlignment(Pos.CENTER);
        ProgressIndicator progressBar = new ProgressIndicator();
        progressBar.setVisible(true);
        progressBar.setMinSize(100, 100);
        progressBar.setStyle("-fx-font-size: 24;-fx-font-weight: bold;");

        Label label = new Label("");
        label.setFont(Font.font(null, FontWeight.BOLD, 24));

        Button button = new Button("Запустить процесс");
        button.setFont(Font.font(null, FontWeight.BOLD, 24));
        button.setOnAction(event -> {
            button.setDisable(true);
            Task<Integer> task = new Task<Integer>() {
                int result = 50;

                @Override
                protected Integer call() throws Exception {
                    for (int i = 0; i < 100; i += 5) {
                        if (isCancelled()) break;
                        updateMessage(String.format("i = %d", i));
                        updateProgress(i, 100);
                        Thread.sleep(500);
                    }
                    Platform.runLater(() -> {
                        PauseTransition pauseTransition = new PauseTransition(Duration.millis(200.));
                        pauseTransition.setOnFinished(e -> {
                            progressBar.progressProperty().unbind();
                            label.textProperty().unbind();
                            progressBar.visibleProperty().unbind();
                            button.setDisable(false);
                        });
                        pauseTransition.play();
                    });
                    return result;
                }

                @Override
                protected void succeeded() {
                    super.succeeded();
                    System.out.println(String.format("result = %d", getValue()));
                    updateMessage("Задача успешно завершена!");
                }
            };

            progressBar.progressProperty().bind(task.progressProperty());
            progressBar.visibleProperty().bind(task.runningProperty());
            label.textProperty().bind(task.messageProperty());

            Thread t = new Thread(task);
            t.start();
        });

        root.getChildren().addAll(label, progressBar, button);
        Scene scene = new Scene(root, 450, 250);
        primaryStage.setTitle("Test Task");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
