package ch11;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.converter.NumberStringConverter;

import java.text.NumberFormat;

public class TestBindBiderectional extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Rectangle rect = new Rectangle(0, 0, 100, 100);
        rect.setFill(Color.RED);
        rect.setArcHeight(10);
        rect.setArcWidth(10);

        Pane pane = new Pane();
        pane.getChildren().addAll(rect);

        Slider slider = new Slider(0, 350, 1);
        slider.setShowTickMarks(true);
        slider.setMajorTickUnit(1);
        slider.valueProperty().bindBidirectional(rect.xProperty());

        TextField textField = new TextField();
        textField.setFont(Font.font(null, FontWeight.BOLD, 24));
        textField.textProperty().bindBidirectional(slider.valueProperty(), new NumberStringConverter());

        Text text = new Text();
        text.setFont(Font.font(null, FontWeight.BOLD, 24));
        text.textProperty().bindBidirectional(slider.valueProperty(), NumberFormat.getInstance());

        VBox root = new VBox(30);
        root.setAlignment(Pos.BASELINE_LEFT);
        root.setFillWidth(true);
        root.getChildren().addAll(pane, slider, textField, text);

        Scene scene = new Scene(root, 450, 350);

        primaryStage.setTitle("Test Change Listener");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
