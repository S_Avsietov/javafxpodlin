package ch11;

import javafx.application.Application;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestService extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox root = new VBox(30);
        root.setAlignment(Pos.CENTER);
        ProgressIndicator progressBar = new ProgressIndicator();
        progressBar.setVisible(true);
        progressBar.setMinSize(100, 100);
        progressBar.setStyle("-fx-font-size: 24;-fx-font-weight: bold;");

        Label label = new Label("");
        label.setFont(Font.font(null, FontWeight.BOLD, 24));

        Button button = new Button("Запустить процесс");
        button.setFont(Font.font(null, FontWeight.BOLD, 24));

        MyService service = new MyService();
        ExecutorService servicePool = Executors.newFixedThreadPool(1);
        service.setExecutor(servicePool);//Иначе сервис будет выполнятся демоном
        service.setOnSucceeded(event -> {
            label.textProperty().unbind();
            label.setText("Задача успешно решена " + event.getSource().getValue());
            button.setDisable(false);
            service.reset();
        });
        service.setOnRunning(event -> {
            label.textProperty().bind(service.messageProperty());
        });
        progressBar.progressProperty().bind(service.progressProperty());
        progressBar.visibleProperty().bind(service.runningProperty());
        button.setOnAction(event -> {
            if (service.getState() == Worker.State.READY) {
                button.setDisable(true);
                service.start();
            } else
                System.out.println("Статус " + service.getState());
        });

        root.getChildren().addAll(label, progressBar, button);
        Scene scene = new Scene(root, 450, 250);
        primaryStage.setTitle("Test Service");
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> {
            service.cancel();
            servicePool.shutdown();
        });
    }

    public static class MyService extends Service<Integer> {
        @Override
        protected Task<Integer> createTask() {
            return new Task<Integer>() {
                int result = 50;

                @Override
                protected Integer call() throws Exception {
                    for (int i = 0; i < 100; i += 5) {
                        if (isCancelled()) break;
                        updateMessage(String.format("i = %d", i));
                        updateProgress(i, 100);
                        Thread.sleep(500);
                    }
                    return result;
                }
            };
        }
    }
}
